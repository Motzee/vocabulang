<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Theme;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ThemeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user = $manager->getRepository(User::class)->find(1);

        $themes = [
            [
                'Couleurs',
                'Farben'
            ],
            [
                'Animaux',
                'Tiere'
            ]
        ] ;
        
        foreach ($themes as $key => $aTheme) {
            $theme = new Theme();
            $theme->setLabelFrFr($aTheme[0])
                ->setLabelDeDe($aTheme[1])
                ->setCreatedBy($user);
            $manager->persist($theme);
        }

        $manager->flush();
    }
}
