<?php

namespace App\Entity;

use App\Entity\Theme;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\WordRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: WordRepository::class)]
class Word
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $grammatical_class = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $fr_label = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $de_label = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $en_label = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $eo_label = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $br_label = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $path_illustration = null;

    #[ORM\ManyToMany(targetEntity: Theme::class, inversedBy: 'words')]
    private Collection $themes;

    public function __construct()
    {
        $this->themes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrammaticalClass(): ?string
    {
        return $this->grammatical_class;
    }

    public function setGrammaticalClass(?string $grammatical_class): self
    {
        $this->grammatical_class = $grammatical_class;

        return $this;
    }

    public function getFrLabel(): ?string
    {
        return $this->fr_label;
    }

    public function setFrLabel(?string $fr_label): self
    {
        $this->fr_label = $fr_label;

        return $this;
    }

    public function getDeLabel(): ?string
    {
        return $this->de_label;
    }

    public function setDeLabel(?string $de_label): self
    {
        $this->de_label = $de_label;

        return $this;
    }

    public function getEnLabel(): ?string
    {
        return $this->en_label;
    }

    public function setEnLabel(?string $en_label): self
    {
        $this->en_label = $en_label;

        return $this;
    }

    public function getEoLabel(): ?string
    {
        return $this->eo_label;
    }

    public function setEoLabel(?string $eo_label): self
    {
        $this->eo_label = $eo_label;

        return $this;
    }

    public function getBrLabel(): ?string
    {
        return $this->br_label;
    }

    public function setBrLabel(?string $br_label): self
    {
        $this->br_label = $br_label;

        return $this;
    }

    public function getPathIllustration(): ?string
    {
        return $this->path_illustration;
    }

    public function setPathIllustration(?string $path_illustration): self
    {
        $this->path_illustration = $path_illustration;

        return $this;
    }

    /**
     * @return Collection<int, Theme>
     */
    public function getThemes(): Collection
    {
        return $this->themes;
    }

    public function addTheme(Theme $theme): self
    {
        if (!$this->themes->contains($theme)) {
            $this->themes->add($theme);
        }

        return $this;
    }

    public function removeTheme(Theme $theme): self
    {
        $this->themes->removeElement($theme);

        return $this;
    }
}
