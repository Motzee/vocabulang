<?php

namespace App\DataFixtures;

use App\Entity\Word;
use App\Entity\Theme;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class WordFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        /*$themes = $manager->getRepository(Theme::class)->findAll();
         $themesIdByFr = [] ;
         foreach($themes as $aTheme) {
         $themesIdByFr[$aTheme->getLabelFrFr()] = $aTheme->getId() ;
         }*/

        $words = [
            ["rouge", "rot", "red", "ruĝa", "ruz", "adjectif"],
            ["vert", "grün", "green", "verda", "gwer", "adjectif"],
            ["chat", "Katze", "cat", "kato", "kazh", "nom"],
            ["chien", "Hund", "dog", "hundo", "ci", "nom"]
        ];

        foreach ($words as $key => $aWordDetails) {
            $word = new Word();
            $word->setGrammaticalClass($aWordDetails[5])
                ->setFrLabel($aWordDetails[0])
                ->setDeLabel($aWordDetails[1])
                ->setEnLabel($aWordDetails[2])
                ->setEoLabel($aWordDetails[3])
                ->setBrLabel($aWordDetails[4]);
            $manager->persist($word);
        }

        $manager->flush();
    }
}