/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/base.css';
import './styles/atomic.css';
import './styles/components/ruban_demo.css';
import './styles/components/btn_link.css';
import './styles/components/card_theme.css';
import './styles/theme.css';

// start the Stimulus application
import './bootstrap';
