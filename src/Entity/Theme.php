<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ThemeRepository;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use App\Entity\User ;

#[ORM\Entity(repositoryClass: ThemeRepository::class)]
class Theme
{
    use TimestampableEntity;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $label_fr_fr = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $label_de_de = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $path_logo = null;

    #[ORM\Column(nullable: true, options: [
        "default" => true
    ])]
    private ?bool $is_public = null;

    #[ORM\ManyToOne(inversedBy: 'themesCreated')]
    #[ORM\JoinColumn(nullable: false)]
    private ?user $created_by = null;

    #[ORM\ManyToMany(targetEntity: Word::class, mappedBy: 'themes')]
    private Collection $words;

    public function __construct()
    {
        $this->words = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabelFrFr(): ?string
    {
        return $this->label_fr_fr;
    }

    public function setLabelFrFr(?string $label_fr_fr): self
    {
        $this->label_fr_fr = $label_fr_fr;

        return $this;
    }

    public function getLabelDeDe(): ?string
    {
        return $this->label_de_de;
    }

    public function setLabelDeDe(?string $label_de_de): self
    {
        $this->label_de_de = $label_de_de;

        return $this;
    }

    public function getPathLogo(): ?string
    {
        return $this->path_logo;
    }

    public function setPathLogo(?string $path_logo): self
    {
        $this->path_logo = $path_logo;

        return $this;
    }

    public function isIsPublic(): ?bool
    {
        return $this->is_public;
    }

    public function setIsPublic(bool $is_public): self
    {
        $this->is_public = $is_public;

        return $this;
    }

    public function getCreatedBy(): ?user
    {
        return $this->created_by;
    }

    public function setCreatedBy(?user $created_by): self
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * @return Collection<int, Word>
     */
    public function getWords(): Collection
    {
        return $this->words;
    }

    public function addWord(Word $word): self
    {
        if (!$this->words->contains($word)) {
            $this->words->add($word);
            $word->addTheme($this);
        }

        return $this;
    }

    public function removeWord(Word $word): self
    {
        if ($this->words->removeElement($word)) {
            $word->removeTheme($this);
        }

        return $this;
    }
}
