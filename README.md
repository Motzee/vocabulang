STATUS : IN PROGRESS
COPYLEFT : CC-ByNCSA

Vocabulang is an app to help learning languages, with a tool to add its own words and sentences, write and share lessons, etc.

Technos :
- Symfony 5, Twig, Doctrine


# Useful
- run tests server :
`symfony server:start`

- generat build (CSS and JS)
`npm run dev`

# Deploy for dev environment
1. Clone project
2. Into the folder, run commands to download libraries for front and back :
```
composer update
npm install
```
3. Generate the build (files for front resources)
```
npm run dev
```
4. Create an SQL user with permissions for the future batabase named "vocabulang"
5. Create a .env.local and complete informations for mail sending and database access
6. Create the database :
```
php bin/console doctrine:database:create
```
7. Run migrations to generate tables :
```
php bin/console doctrine:migrations:migrate
```
8. Go on the web interface and create an user
9. Give Admin status to this user with this SQL request :
```
UPDATE user SET roles = '["ROLE_ADMIN"]' WHERE id = 1;
```
10. Run fixtures to populate the database (without deleting user created) :
```
php bin/console doctrine:fixtures:load --append
```