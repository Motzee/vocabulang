<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\WordRepository;
use App\Repository\ThemeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * List tools for admin
     */
    #[Route('/admin', name: 'admin_home')]
    #[IsGranted('ROLE_ADMIN')]
    public function index(): Response
    {
        return $this->render('admin/index.twig', []);
    }

    /**
     * List of users
     */
    #[Route('/admin/users', name: 'admin_users')]
    #[IsGranted('ROLE_ADMIN')]
    public function users(UserRepository $userRepository): Response
    {
        
        return $this->render('admin/users.twig', [
            'users' => $userRepository->findAll()
        ]);
    }

    /**
     * List of themes
     */
    #[Route('/admin/themes', name: 'admin_themes')]
    #[IsGranted('ROLE_ADMIN')]
    public function themes(ThemeRepository $themeRepository): Response
    {
        return $this->render('admin/themes.twig', [
            'themes' => $themeRepository->findAll(),
        ]);
    }

    /**
     * 
     */
    #[Route('/admin/words', name: 'admin_words')]
    #[IsGranted('ROLE_ADMIN')]
    public function words(WordRepository $wordRepository): Response
    {
        return $this->render('admin/words.twig', [
            'words' => $wordRepository->findAll(),
        ]);
    }

    #[Route('/admin/sentences', name: 'admin_sentences')]
    #[IsGranted('ROLE_ADMIN')]
    public function sentences(): Response
    {
        return $this->render('admin/sentences.twig', []);
    }

    #[Route('/admin/lessons', name: 'admin_lessons')]
    #[IsGranted('ROLE_ADMIN')]
    public function lessons(): Response
    {
        return $this->render('admin/lessons.twig', []);
    }
}
